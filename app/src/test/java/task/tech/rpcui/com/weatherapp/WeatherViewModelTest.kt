package task.tech.rpcui.com.weatherapp

import org.junit.Before
import org.junit.Test
import org.mockito.*
import org.mockito.Mockito.*
import task.tech.rpcui.com.weatherapp.network.GetForecastTask
import task.tech.rpcui.com.weatherapp.viewmodel.WeatherViewModel

class WeatherViewModelTest {

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun testIfTaskIsExecuted() {
        var viewModel = WeatherViewModel()
        var mockedTask = Mockito.mock(GetForecastTask::class.java)
        viewModel.getForecast(mockedTask)

        Mockito.verify(mockedTask, times(1)).execute()
    }

}