package task.tech.rpcui.com.weatherapp.model

import android.os.Parcel
import android.os.Parcelable

data class Weather(val desc: String, val weatherType: Int): Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readInt())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(desc)
        parcel.writeInt(weatherType)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Weather> {
        override fun createFromParcel(parcel: Parcel): Weather {
            return Weather(parcel)
        }

        override fun newArray(size: Int): Array<Weather?> {
            return arrayOfNulls(size)
        }
    }

}

class Forecast(val tempMax: Int, val tempMin: Int, val dayWeather: Weather, val nightWeather: Weather, val date: Long) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readInt(),
            parcel.readInt(),
            parcel.readParcelable(Weather::class.java.classLoader),
            parcel.readParcelable(Weather::class.java.classLoader),
            parcel.readLong())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(tempMax)
        parcel.writeInt(tempMin)
        parcel.writeParcelable(dayWeather, flags)
        parcel.writeParcelable(nightWeather, flags)
        parcel.writeLong(date)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Forecast> {
        override fun createFromParcel(parcel: Parcel): Forecast {
            return Forecast(parcel)
        }

        override fun newArray(size: Int): Array<Forecast?> {
            return arrayOfNulls(size)
        }
    }

}