package task.tech.rpcui.com.weatherapp.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import task.tech.rpcui.com.weatherapp.model.*
import task.tech.rpcui.com.weatherapp.network.GetForecastTask

class WeatherViewModel : ViewModel() {

    private var forecastLiveData = MutableLiveData<ArrayList<Forecast>>()
    private var throwableLiveData = MutableLiveData<Throwable>()

    val defaultCity: City = City.MANILA

    fun forecastData(): LiveData<ArrayList<Forecast>> {
        return forecastLiveData
    }

    fun throwableExceptions(): LiveData<Throwable> {
        return throwableLiveData
    }

    fun getForecast(forecastTask: GetForecastTask) {
        forecastTask.execute()
    }

    fun createForecastTask(locationKey: Int, apiKey: String): GetForecastTask {
        return GetForecastTask(locationKey = locationKey, apiKey = apiKey)
                .addResultLiveData(forecastLiveData)
                .addThrowableLiveData(throwableLiveData)
    }

}