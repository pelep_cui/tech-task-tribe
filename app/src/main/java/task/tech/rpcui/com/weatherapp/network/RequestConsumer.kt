package task.tech.rpcui.com.weatherapp.network

interface RequestConsumer<T> {
    fun onResponse(t: T)
}