package task.tech.rpcui.com.weatherapp

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import task.tech.rpcui.com.weatherapp.model.Weather
import kotlinx.android.synthetic.main.activity_weather_details.*
import task.tech.rpcui.com.weatherapp.model.Forecast
import task.tech.rpcui.com.weatherapp.utils.WeatherUtils

class WeatherDetailsActivity : AppCompatActivity() {

    companion object  {

        const val KEY_FORECAST = "forecast"

        fun createIntent(context: Context, forecast: Forecast): Intent {
            var intent = Intent(context, WeatherDetailsActivity::class.java)
            intent.putExtra(KEY_FORECAST, forecast)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_weather_details)

        var intent = intent
        if (intent.hasExtra(KEY_FORECAST)) {
            var forecast = intent.getParcelableExtra<Forecast>(KEY_FORECAST)
            showWeatherDetails(forecast)
        }
    }

    private fun showWeatherDetails(forecast: Forecast) {
        var utils = WeatherUtils()

        dayInWeek.text = utils.formatDate(forecast.date, "EEEE")
        dayWeatherIcon.setImageResource(utils.getWeatherIcon(forecast.dayWeather.weatherType))
        dayWeatherDesc.text = forecast.dayWeather.desc

        nightWeatherIcon.setImageResource(utils.getWeatherIcon(forecast.nightWeather.weatherType))
        nightWeatherDesc.text = forecast.nightWeather.desc

        maxTemperature.text = "MAX ${utils.toFarenheit(forecast.tempMax)}"
        minTemperature.text = "MIN ${utils.toFarenheit(forecast.tempMax)}"
    }
}