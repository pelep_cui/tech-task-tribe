package task.tech.rpcui.com.weatherapp.network

import android.arch.lifecycle.MutableLiveData
import android.os.AsyncTask
import task.tech.rpcui.com.weatherapp.model.Forecast
import org.json.JSONException
import org.json.JSONObject
import task.tech.rpcui.com.weatherapp.model.Weather
import java.io.BufferedReader
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL


class GetForecastTask(val locationKey: Int, val apiKey: String) : AsyncTask<Void, Void, ForecastResponse>() {

    private var forecastLiveData: MutableLiveData<ArrayList<Forecast>>? = null
    private var throwableLiveData: MutableLiveData<Throwable>? = null

    override fun doInBackground(vararg param: Void?): ForecastResponse {

        val forecasts = ArrayList<Forecast>()
        var statusCode = 0
        val throwable: Throwable?

        val url = URL("http://dataservice.accuweather.com/forecasts/v1/daily/5day/$locationKey?apikey=$apiKey")
        var connection: HttpURLConnection? = null
        try {
            connection = url.openConnection() as HttpURLConnection
            connection.requestMethod = "GET"
            connection.setRequestProperty("content-type", "application/json;  charset=utf-8")
            connection.setRequestProperty("Content-Language", "en-US")
            connection.useCaches = false
            connection.doInput = true
            connection.doOutput = false
            statusCode = connection.responseCode
            val inputStream: InputStream = if (statusCode != HttpURLConnection.HTTP_OK)
                connection.errorStream
            else
                connection.inputStream
            statusCode = connection.responseCode

            val allText = inputStream.bufferedReader().use(BufferedReader::readText)
            val data = parseReponseFromData(allText)
            return ForecastResponse(statusCode, null, data)
        }
        catch (e: Exception) {
            throwable = e
        }
        catch (e: JSONException) {
            throwable = e
        }
        finally {
            connection?.disconnect()
        }
        return ForecastResponse(statusCode, throwable, forecasts)
    }

    override fun onPostExecute(result: ForecastResponse) {
        super.onPostExecute(result)
        if (result.isSuccessful()) {
            forecastLiveData?.postValue(result.forecasts)
        }
        else {
            throwableLiveData?.postValue(result.throwable ?: Throwable("Request failed: " + result.statusCode))
        }
    }

    private fun parseReponseFromData(data: String): ArrayList<Forecast>{
        val forecasts = ArrayList<Forecast>()
        val json = JSONObject(data)
        val list = json.optJSONArray("DailyForecasts")

        if (list != null) {
            for (i in 0 until list.length()) {
                val dailyData = list.getJSONObject(i)
                val date = dailyData.optLong("EpochDate", 0L)
                val temperature = dailyData.getJSONObject("Temperature")
                val minTemp = temperature.getJSONObject("Minimum").optInt("Value")
                val maxTemp = temperature.getJSONObject("Maximum").optInt("Value")
                val dayWeatherData = dailyData.getJSONObject("Day")
                val dayWeatherDesc = dayWeatherData.optString("IconPhrase")
                val dayWeatherType = dayWeatherData.optInt("Icon")
                val dayWeather = Weather(desc = dayWeatherDesc, weatherType = dayWeatherType)
                val nightWeatherData = dailyData.getJSONObject("Night")
                val nightWeatherDesc = nightWeatherData.optString("IconPhrase")
                val nightWeatherType = nightWeatherData.optInt("Icon")
                val nightWeather = Weather(desc = nightWeatherDesc, weatherType = nightWeatherType)
                val forecast = Forecast(tempMax = maxTemp, tempMin = minTemp, dayWeather = dayWeather, nightWeather = nightWeather, date = date)
                forecasts.add(forecast)
            }
        }
        return forecasts
    }

    fun addResultLiveData(forecastLiveData: MutableLiveData<ArrayList<Forecast>>): GetForecastTask {
        this.forecastLiveData = forecastLiveData
        return this
    }

    fun addThrowableLiveData(throwableLiveData: MutableLiveData<Throwable>): GetForecastTask {
        this.throwableLiveData = throwableLiveData
        return this
    }


}