package task.tech.rpcui.com.weatherapp.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import task.tech.rpcui.com.weatherapp.R
import task.tech.rpcui.com.weatherapp.model.Forecast
import kotlinx.android.synthetic.main.item_forecast_overview.view.*
import task.tech.rpcui.com.weatherapp.utils.WeatherUtils
import java.util.*

class ForecastListAdapter(private var list: ArrayList<Forecast>) : RecyclerView.Adapter<ForecastViewHolder>() {

    private var itemClickListener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ForecastViewHolder {
        var view = LayoutInflater.from(parent.context).inflate(R.layout.item_forecast_overview, parent, false)
        return ForecastViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ForecastViewHolder, position: Int) {

        var utils = WeatherUtils()

        var forecast = list[position]
        holder.bindForecast(list[position])
        holder.bindDayInWeek(utils.formatDate(timestamp = forecast.date, format="EEEE"))
        holder.bindIcon(utils.getWeatherIcon(weatherId = forecast.dayWeather.weatherType))
        holder.bindTemp(utils.toFarenheit(forecast.tempMax))
        holder.setItemClickListener(itemClickListener)
    }

    fun updateItems(list: ArrayList<Forecast>) {
        this.list.clear()
        this.list.addAll(list)
        notifyItemRangeInserted(0, list.size)
    }

    fun setItemClickListener(itemClickListener: OnItemClickListener) {
        this.itemClickListener = itemClickListener
    }

}

interface OnItemClickListener {
    fun onItemClicked(forecast: Forecast)
}

class ForecastViewHolder(val view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {

    init {
        view.setOnClickListener(this)
    }

    private var listener: OnItemClickListener? = null
    private lateinit var forecast: Forecast

    override fun onClick(view: View?) {
        listener?.onItemClicked(forecast)
    }

    fun setItemClickListener(listener: OnItemClickListener?) {
        this.listener = listener
    }

    fun bindForecast(forecast: Forecast) {
        this.forecast = forecast
    }

    fun bindDayInWeek(day: String) {
        view.dayInWeek.text = day
    }

    fun bindIcon(icon: Int) {
        view.weatherIcon.setImageResource(icon)
    }

    fun bindTemp(temp: CharSequence) {
        view.temp.text = temp
    }
}