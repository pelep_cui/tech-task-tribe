package task.tech.rpcui.com.weatherapp.utils

import task.tech.rpcui.com.weatherapp.R
import java.text.SimpleDateFormat
import java.util.*


class WeatherUtils {

    fun formatDate(timestamp: Long, format: String): String {
        val sdf = SimpleDateFormat(format)
        val d = Date(timestamp * 1000)
        return sdf.format(d)
    }

    fun getWeatherIcon(weatherId: Int): Int {
        var icon = 0
        when (weatherId) {
            1 -> icon = R.drawable.weather_1_icon
            2 -> icon = R.drawable.weather_2_icon
            3 -> icon = R.drawable.weather_3_icon
            4 -> icon = R.drawable.weather_4_icon
            5 -> icon = R.drawable.weather_5_icon
            6 -> icon = R.drawable.weather_6_icon
            7 -> icon = R.drawable.weather_7_icon
            8 -> icon = R.drawable.weather_8_icon
            11 -> icon = R.drawable.weather_11_icon
            12 -> icon = R.drawable.weather_12_icon
            13 -> icon = R.drawable.weather_13_icon
            14 -> icon = R.drawable.weather_14_icon
            15 -> icon = R.drawable.weather_15_icon
            16 -> icon = R.drawable.weather_16_icon
            17 -> icon = R.drawable.weather_17_icon
            18 -> icon = R.drawable.weather_18_icon
            19 -> icon = R.drawable.weather_19_icon
            20 -> icon = R.drawable.weather_20_icon
            21 -> icon = R.drawable.weather_21_icon
            22 -> icon = R.drawable.weather_22_icon
            23 -> icon = R.drawable.weather_23_icon
            24 -> icon = R.drawable.weather_24_icon
            25 -> icon = R.drawable.weather_25_icon
            26 -> icon = R.drawable.weather_26_icon
            29 -> icon = R.drawable.weather_29_icon
            30 -> icon = R.drawable.weather_30_icon
            31 -> icon = R.drawable.weather_31_icon
            32 -> icon = R.drawable.weather_32_icon
            33 -> icon = R.drawable.weather_33_icon
            34 -> icon = R.drawable.weather_34_icon
            35 -> icon = R.drawable.weather_35_icon
            36 -> icon = R.drawable.weather_36_icon
            37 -> icon = R.drawable.weather_37_icon
            38 -> icon = R.drawable.weather_38_icon
            39 -> icon = R.drawable.weather_39_icon
            40 -> icon = R.drawable.weather_40_icon
            42 -> icon = R.drawable.weather_42_icon
            43 -> icon = R.drawable.weather_43_icon
            44 -> icon = R.drawable.weather_44_icon
        }
        return icon
    }

    fun toFarenheit(temp: Int): CharSequence {
        return  "$temp \u2109"
    }
}