package task.tech.rpcui.com.weatherapp.network

import task.tech.rpcui.com.weatherapp.model.Forecast


class ForecastResponse(val statusCode: Int, val throwable: Throwable?, val forecasts: ArrayList<Forecast>?) {


    fun isSuccessful(): Boolean {
        return statusCode in 200..300
    }

 }