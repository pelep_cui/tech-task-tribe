package task.tech.rpcui.com.weatherapp

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import task.tech.rpcui.com.weatherapp.adapter.ForecastListAdapter
import task.tech.rpcui.com.weatherapp.adapter.OnItemClickListener
import task.tech.rpcui.com.weatherapp.model.Forecast
import task.tech.rpcui.com.weatherapp.utils.WeatherUtils
import task.tech.rpcui.com.weatherapp.viewmodel.WeatherViewModel
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: WeatherViewModel
    private lateinit var listAdapter: ForecastListAdapter
    private lateinit var layoutManager: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProviders.of(this).get(WeatherViewModel::class.java)
        initializeList()
        initializeForecastData()
        getForecast()
    }


    private fun initializeList() {
        listAdapter = ForecastListAdapter(ArrayList())
        layoutManager = LinearLayoutManager(this)
        listForecasts.adapter = listAdapter
        listForecasts.layoutManager = layoutManager
        listAdapter.setItemClickListener(object: OnItemClickListener {
            override fun onItemClicked(forecast: Forecast) {
                showForecastDetails(forecast)
            }
        })
    }

    private fun initializeForecastData() {
        viewModel.forecastData().observe(this, Observer { it ->
            run {
                if (it != null && it.size > 0) {
                    val forecast = it.removeAt(0)
                    displayFeaturedForecast(forecast)
                    displayForecast(it)
                }
            }
        })
        viewModel.throwableExceptions().observe(this, Observer { it ->
            kotlin.run {
                retry.visibility = View.VISIBLE
                progressIndicator.visibility = View.GONE
                retry.setOnClickListener {
                    retryRequest()
                }
                Snackbar.make(rootLayout, "An unexpected error occurred: ${it!!.message}", Snackbar.LENGTH_LONG).show()
            }
        })
    }

    private fun retryRequest() {
        progressIndicator.visibility = View.VISIBLE
        retry.visibility = View.GONE
        retry.setOnClickListener(null)
        getForecast()
    }

    private fun getForecast() {
        val forecastTask = viewModel.createForecastTask(viewModel.defaultCity.id, "FEbg6qZVV9KZhM4ITJNka6gJPDlK5q2M")
        viewModel.getForecast(forecastTask)
    }

    private fun displayFeaturedForecast(forecast: Forecast) {
        featuredCity.text = viewModel.defaultCity.name
        val utils = WeatherUtils()
        dayWeatherIcon.setImageResource(utils.getWeatherIcon(forecast.dayWeather.weatherType))
        dayWeatherDesc.text = forecast.dayWeather.desc

        nightWeatherIcon.setImageResource(utils.getWeatherIcon(forecast.dayWeather.weatherType))
        nightWeatherDesc.text = forecast.dayWeather.desc

        featuredDayInWeek.text = utils.formatDate(forecast.date, "EEEE")

        headerDay.visibility = View.VISIBLE
        headerNight.visibility = View.VISIBLE
        progressIndicator.visibility = View.GONE
    }

    private fun displayForecast(forecasts: ArrayList<Forecast>) {
        listAdapter.updateItems(forecasts)
    }

    private fun showForecastDetails(forecast: Forecast) {
        val intent = WeatherDetailsActivity.createIntent(this,forecast)
        startActivityForResult(intent, 0)
    }


}